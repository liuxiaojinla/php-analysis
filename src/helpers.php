<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: 晋<657306123@qq.com>
 */
if (!function_exists('analysis_keywords')) {
	/**
	 * 关键字分词
	 *
	 * @param string $keywords
	 * @param int    $resultNum 最大返回条数
	 * @param int    $holdLength 保留字数
	 * @return array
	 */
	function analysis_keywords($keywords, $resultNum = 5, $holdLength = 48, $options = []) {
		return \Xin\Analysis\Analysis::analysis($keywords, $resultNum, $holdLength, $options);
	}
}

if (!function_exists('keywords_build_sql')) {
	/**
	 * 编译查询关键字SQL
	 *
	 * @param string $keywords
	 * @return array
	 */
	function keywords_build_sql($keywords) {
		$keywords = analysis_keywords($keywords);

		return array_map(function ($item) {
			return "%{$item}%";
		}, $keywords);
	}
}
